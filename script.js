var counter = 0; 

function myCounter() {
    counter++;
    document.getElementById("counterDisplay").innerText = counter;
    console.log("Vous avez cliqué !", counter, "fois");
}

function changeFontSize() {
    var newSize = document.getElementById('textSize').value + "px";
    var paragraphs = document.getElementsByTagName('p');

    for(var i=0; i<paragraphs.length; i++) {
        paragraphs[i].style.fontSize = newSize; 
    }
}

document.getElementById("clickCounter").addEventListener('click' , myCounter)


document.getElementById("textSize").addEventListener('input', changeFontSize)